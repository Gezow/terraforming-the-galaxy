# Creating IAM role for Kubernetes clusters to make calls to other AWS services on your behalf to manage the resources that you use with the service.

terraform {
  backend "http" {
  }
}

provider "aws" {
}

resource "aws_iam_role" "iam-role-eks-cluster" {
  name = "GitLab-TestEksClusterRole"
  assume_role_policy = <<POLICY
{
 "Version": "2012-10-17",
 "Statement": [
   {
   "Effect": "Allow",
   "Principal": {
    "Service": "eks.amazonaws.com"
   },
   "Action": "sts:AssumeRole"
   }
  ]
 }
POLICY
}

# Attaching the EKS-Cluster policies to the terraformekscluster role.

resource "aws_iam_role_policy_attachment" "eks-cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = "${aws_iam_role.iam-role-eks-cluster.name}"
}

# Create VPC

resource "aws_vpc" "my-vpc-0" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "left"
  }
}

resource "aws_default_route_table" "def-rt" {
  default_route_table_id = aws_vpc.my-vpc-0.main_route_table_id

  tags = {
    Name = "my-main-rt"
  }
}

resource "aws_subnet" "GitLab-testSubnet" {
  vpc_id = aws_vpc.my-vpc-0.id
  availability_zone = "eu-central-1a"
  map_public_ip_on_launch = true
  cidr_block = "10.0.0.0/20"
}

resource "aws_subnet" "GitLab-testSubnet1" {
  vpc_id = aws_vpc.my-vpc-0.id
  availability_zone = "eu-central-1b"
  map_public_ip_on_launch = true
  cidr_block = "10.0.16.0/20"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.my-vpc-0.id
}

resource "aws_route" "rt" {
  route_table_id = aws_vpc.my-vpc-0.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.gw.id
}


# Security group for network traffic to and from AWS EKS Cluster.

resource "aws_security_group" "eks-cluster" {
  name        = "GitLab-TestEksClusterSg"
  vpc_id      = aws_vpc.my-vpc-0.id

# Egress allows Outbound traffic from the EKS cluster to the  Internet 

  egress {                   # Outbound Rule
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
# Ingress allows Inbound traffic to EKS cluster from the  Internet 

  ingress {                  # Inbound Rule
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

# Creating the EKS cluster

resource "aws_eks_cluster" "eks_cluster" {
  name     = "GitLab-TestEksCluster"
  role_arn =  "${aws_iam_role.iam-role-eks-cluster.arn}"
  version  = "1.21"

# Adding VPC Configuration

  vpc_config {             # Configure EKS with vpc and network settings 
   security_group_ids = ["${aws_security_group.eks-cluster.id}"]
   subnet_ids         = [aws_subnet.GitLab-testSubnet.id,aws_subnet.GitLab-testSubnet1.id] 
    }

}

# Creating IAM role for EKS nodes to work with other AWS Services. 


resource "aws_iam_role" "eks_nodes" {
  name = "GitLab-TestEksClusterNodeGroup"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

# Attaching the different Policies to Node Members.

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_nodes.name
}

# Create EKS cluster node group

resource "aws_eks_node_group" "node" {
  version  = "1.21"
  cluster_name    = aws_eks_cluster.eks_cluster.name
  node_group_name = "GitLab-testNodeGroup"
  node_role_arn   = aws_iam_role.eks_nodes.arn
  subnet_ids      = [aws_subnet.GitLab-testSubnet.id,aws_subnet.GitLab-testSubnet1.id]
  instance_types  = ["t2.micro"]

  scaling_config {
    desired_size = 10
    max_size     = 12
    min_size     = 8
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]
}
